package com.ubagroup.gatewayservice.api;

import org.springframework.web.server.ServerWebExchange;

import com.ubagroup.utils.model.Client;
import com.ubagroup.utils.model.Resource;

public interface SecurityServiceApi {

	public Client authenticate(ServerWebExchange exchange, Resource resource);
	
	public boolean isAuthorized(Client client, Resource resource);
	
	public ServerWebExchange setDownstreamExchangeAuth(ServerWebExchange exchange, Resource resource);
}
