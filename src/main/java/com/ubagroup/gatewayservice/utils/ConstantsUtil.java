package com.ubagroup.gatewayservice.utils;

public class ConstantsUtil {

	public static final String UNAUTHORIZED_RESPONSE = "{\"status\":\"failed\",\"message\":\"Authentication failed.\"}";
	public static final String FORBIDDEN_RESPONSE = "{\"status\":\"failed\",\"message\":\"You are not permitted to access the requested resource on this system.\"}";
	public static final String UNKNOWN_RESOURCE_RESPONSE = "{\"status\":\"failed\",\"message\":\"Resource Not Found\"}";
	public static final String RESOURCE_DISABLED = "{\"status\":\"failed\",\"message\":\"Resource is disabled.\"}";
	
	public static final String AUTHORIZATION = "Authorization";
	public static final String BASIC = "Basic";
	public static final String DOWNSTREAMAUTHORIZATION = "DownStreamAuthorization";

}
