package com.ubagroup.gatewayservice.filter;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

import java.nio.charset.StandardCharsets;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.ubagroup.gatewayservice.api.SecurityServiceApi;
import com.ubagroup.gatewayservice.utils.ConstantsUtil;
import com.ubagroup.utils.model.Client;
import com.ubagroup.utils.model.Resource;
import com.ubagroup.utils.service.ResourceService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class AuthFilter implements GlobalFilter, Ordered {
	private static final Log LOG = LogFactory.getLog(AuthFilter.class);
	
	@Autowired SecurityServiceApi securityService;
	
	@Autowired ResourceService resourceService;
	
	@Value("${allow.unknown.resources}")
	private boolean allowUnknownResources;
	
	@Value("${disable.gateway.auth}")
	private boolean disableGeneralAuth;
	
	@Override
	public int getOrder()
	{
		return Integer.MAX_VALUE;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain)
	{
		try
		{
			if(disableGeneralAuth)
			{
				LOG.debug("General AUTH DISABLED by config. Gateway will not attempt authentication.");
				//get auth headers and add to request
				return chain.filter(exchange);
			}
			
			LOG.debug("General AUTH ENABLED by config. Gateway will attempt authentication.");
			Resource resource = getResource(exchange);
			if(null != resource && resource instanceof Resource)
			{
				if(null != resource.getEnableEndpoint() && resource.getEnableEndpoint().equals(0L))
				{
					LOG.debug("Access to this Resource was disabled, request cannot proceed...");
					LOG.debug("Aborting request.");
					return returnfailedResponse(HttpStatus.FORBIDDEN, ConstantsUtil.RESOURCE_DISABLED, exchange);
				}
				
				if(null != resource.getAuthenticate() && resource.getAuthenticate().equals(0L))
				{
					LOG.debug("Resource does not require authentication...");
					LOG.debug("Setting default auth headers for exchange from resource configurations.");
					exchange = securityService.setDownstreamExchangeAuth(exchange, resource);
					return chain.filter(exchange);
				}
				else
				{
					LOG.debug("Attempting client authentication...");
					Client client = securityService.authenticate(exchange, resource);
					if(null != client && client instanceof Client)
					{
						LOG.debug("Request passed authentication. Attempting authorization....");
						if(null != resource.getAuthorize() && resource.getAuthorize().equals(1L) && securityService.isAuthorized(client, resource))
						{
							LOG.debug("Request passed authorization. Proceeding to process request.....");
							exchange = securityService.setDownstreamExchangeAuth(exchange, resource);
							return chain.filter(exchange);
						}
						LOG.debug("Request failed authorization. Terminating request and returning failed response.....");
						return returnfailedResponse(HttpStatus.FORBIDDEN, ConstantsUtil.FORBIDDEN_RESPONSE, exchange);
					}
					LOG.debug("Request failed authentication. Terminating request and returning failed response.....");
					return returnfailedResponse(HttpStatus.UNAUTHORIZED, ConstantsUtil.UNAUTHORIZED_RESPONSE, exchange);
				}
			}
			
			LOG.debug("Resource is unknown....");
			
			if(allowUnknownResources)
			{
				LOG.debug("Unknown resources are allowed by gateway configuration. Proceeding to allow unknown resource...");
				return chain.filter(exchange);
			}
			
			LOG.debug("Requested resource is not found and request is being terminated...");
			LOG.info("To allow processing of unknow resources, set allow.unknown.resources to true in the properties file.");
			
			return returnfailedResponse(HttpStatus.NOT_FOUND, ConstantsUtil.UNKNOWN_RESOURCE_RESPONSE, exchange);
		}
		catch(Exception e)
		{
			LOG.error("Exception occured while authenticating gateway request...");
			LOG.error("Exception is: ", e);
			return returnfailedResponse(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Something went wrong while attempting gateway request authentication.", exchange);
		}
	}

	private Resource getResource(ServerWebExchange exchange)
	{
		try
		{
			Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
			String host = route.getUri().getHost();
			String path = exchange.getRequest().getURI().getPath();
			path = "/" + host.replace("/", "") + path;
			String method = exchange.getRequest().getMethodValue();
			LOG.debug("About to fetch resource by PATH and METHOD");
			LOG.debug("Method / Path -> " + method + " / " + path);
			return resourceService.findByPathAndMethod(path, method);
		}
		catch(Exception e)
		{
			LOG.error("Exception while retrieving resource...", e);
			return null;
		}
	}

	private Mono<Void> returnfailedResponse(HttpStatus code, String msg, ServerWebExchange exchange)
	{
		LOG.debug("Returning failed error message >>>>> " + msg);
		LOG.debug("Returning failed error code >>>>>>>> HTTP " + code.toString());
		byte[] bytes = msg.getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
        exchange.getResponse().setStatusCode(code);
        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        return exchange.getResponse().writeWith(Flux.just(buffer));
	}

}
