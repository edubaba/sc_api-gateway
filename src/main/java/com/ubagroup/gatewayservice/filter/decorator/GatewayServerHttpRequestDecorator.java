package com.ubagroup.gatewayservice.filter.decorator;

import static reactor.core.scheduler.Schedulers.single;

import java.io.StringWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;

import reactor.core.publisher.Flux;
import java.nio.charset.StandardCharsets;

public class GatewayServerHttpRequestDecorator extends ServerHttpRequestDecorator {

	private final StringWriter cachedCopy = new StringWriter();
	
	private static final Log LOG = LogFactory.getLog(GatewayServerHttpRequestDecorator.class);

    GatewayServerHttpRequestDecorator(ServerHttpRequest delegate) {
        super(delegate);
    }

    @Override
    public Flux<DataBuffer> getBody() {
        return super.getBody()
            .publishOn(single())
            .doOnNext(this::cache)
            .doOnComplete(() -> trace(getDelegate(), cachedCopy.toString()));
    }

    private void cache(DataBuffer buffer) {
        cachedCopy.write(new String(buffer.asByteBuffer().array(), StandardCharsets.UTF_8));
    }

    private void trace(ServerHttpRequest request, String requestBody) {
    	LOG.debug("LOGGING REQUEST BODY: -> ");
        LOG.debug(requestBody);
    }
}
