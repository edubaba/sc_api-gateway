package com.ubagroup.gatewayservice.filter.decorator;

import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;

public class GatewayServerHttpResponseDecorator extends ServerHttpResponseDecorator {

	
//	private static final Log LOG = LogFactory.getLog(GatewayServerHttpRequestDecorator.class);
	
	public GatewayServerHttpResponseDecorator(ServerHttpResponse delegate) {
		super(delegate);
	}

}
