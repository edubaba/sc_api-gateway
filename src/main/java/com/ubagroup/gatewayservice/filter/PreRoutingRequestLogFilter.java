package com.ubagroup.gatewayservice.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.ubagroup.gatewayservice.filter.decorator.GatewayServerWebExchangeDecorator;

import reactor.core.publisher.Mono;

@Component("postLogfilter")
public class PreRoutingRequestLogFilter implements GlobalFilter, Ordered {
	
	@Override
	public int getOrder()
	{
		return Integer.MIN_VALUE;
	}

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain)
	{
		return chain.filter(new GatewayServerWebExchangeDecorator(exchange));
	}

}
