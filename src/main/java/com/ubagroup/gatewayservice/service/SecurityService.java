package com.ubagroup.gatewayservice.service;

import java.util.Base64;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import com.ubagroup.gatewayservice.api.SecurityServiceApi;
import com.ubagroup.gatewayservice.utils.ConstantsUtil;
import com.ubagroup.utils.model.Client;
import com.ubagroup.utils.model.ClientPermission;
import com.ubagroup.utils.model.Resource;
import com.ubagroup.utils.service.ClientPermissionService;
import com.ubagroup.utils.service.ClientService;
import com.ubagroup.utils.service.PermissionService;
import com.ubagroup.utils.service.ResourceService;
import com.ubagroup.utils.util.AppUtils;
import com.ubagroup.utils.util.AuthType;

@Service
public class SecurityService implements SecurityServiceApi {

	@Autowired ClientService clientService;
	
	@Autowired ResourceService resourceService;
	
	@Autowired PermissionService permissionService;
	
	@Autowired ClientPermissionService clientPermissionService;
	
	@Autowired AppUtils utils;
	
	@Value("${gateway.auth.type}") 
	private String authMethod;
	
	@Value("${use.gateway.auth.method}") 
	private boolean useGatewayAuthMethod;
	
	private static final Log LOG = LogFactory.getLog(SecurityService.class);
	
	@Override 
	public Client authenticate(ServerWebExchange exchange, Resource resource)
	{
		String[] auth = getRequestAuthDetails(exchange, resource.getAuthType());
		if(null != auth && auth.length > 1)
		{
			Client client = getClient(auth[0]);
			if(null != client && null != client.getClientSecret() && auth[1].equals(client.getClientSecret()))
				return client;
		}
		return null;
	}

	@Override
	public boolean isAuthorized(Client client, Resource resource)
	{
		if(null != client && null != resource && null != client.getId() && null != resource.getPermissionId())
		{
			List<ClientPermission> cps = clientPermissionService.findByClientId(client.getId());
			if(null != cps)
				for(ClientPermission cp : cps)
					if(null != cp && null != cp.getPermissionId() && cp.getPermissionId().equals(resource.getPermissionId()))
						return true;
		}
		return false;
	}

	@Override
	public ServerWebExchange setDownstreamExchangeAuth(ServerWebExchange exchange, Resource resource)
	{
		if(ConstantsUtil.BASIC.equalsIgnoreCase(resource.getAuthType()))
		{
			String authHeader = ConstantsUtil.BASIC + " " + getBase64String(resource.getAuthUser() + ":" + resource.getAuthPass());
            ServerHttpRequest request = exchange.getRequest();

			request = request.mutate()
                    .header(ConstantsUtil.DOWNSTREAMAUTHORIZATION, authHeader)
                    .build();

            return exchange.mutate().request(request).build();
		}
		
		return exchange;
	}

	private String[] getRequestAuthDetails(ServerWebExchange exchange, String authType)
	{
		if(useGatewayAuthMethod && !utils.isEmptyString(authMethod))
			authType = authMethod;
	
		if(AuthType.BASIC.name().equalsIgnoreCase(authType))
			return getBasicAuthDetails(exchange);
		if(AuthType.JWT.name().equalsIgnoreCase(authType))
			return getJwtAuthDetails(exchange);
		if(AuthType.BEARER.name().equalsIgnoreCase(authType))
			return getBearerAuthDetails(exchange);
		
		return null;
	}
	
	private String[] getBearerAuthDetails(ServerWebExchange exchange)
	{
		// TODO Auto-generated method stub
		return null;
	}

	private String[] getJwtAuthDetails(ServerWebExchange exchange)
	{
		// TODO Auto-generated method stub
		return null;
	}

	private String[] getBasicAuthDetails(ServerWebExchange exchange)
	{
		if(null == exchange)  return null;
		
		try
		{
			List<String> authHeaders = exchange.getRequest().getHeaders().get(ConstantsUtil.AUTHORIZATION);
			String authHeaderString = getStringFromBase64String(authHeaders.get(0).replace("Basic ", ""));
			return authHeaderString.split(":");
		}
		catch(Exception e)
		{
			LOG.error("Exception occured while retrieving request client. ", e);
		}
		
		return null;
	}
	
	private String getStringFromBase64String(String base64String)
	{
		byte[] decodedBytes = Base64.getDecoder().decode(base64String);
		return new String(decodedBytes);
	}
	
	private String getBase64String(String string)
	{
		return Base64.getEncoder().encodeToString(string.getBytes());
	}
	
	private Client getClient(String clientId)
	{
		try
		{
			Client client = clientService.findById(clientId);
			if(null != client) return client;
			return null;
		}
		catch(Exception e)
		{
			LOG.debug("Something went wrong while retrieving client. ", e);
			return null;
		}
	}

}
