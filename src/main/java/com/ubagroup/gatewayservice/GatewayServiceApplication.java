package com.ubagroup.gatewayservice;

import java.util.Arrays;
import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.handler.RoutePredicateHandlerMapping;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.cors.CorsConfiguration;

@SpringBootApplication
public class GatewayServiceApplication {

	@Bean
	DiscoveryClientRouteDefinitionLocator discoverRoutes(DiscoveryClient dc)
	{
		return new DiscoveryClientRouteDefinitionLocator(dc);
	}
	
	@SuppressWarnings("serial")
	@Bean 
	public CorsConfiguration corsConfiguration(RoutePredicateHandlerMapping routePredicateHandlerMapping) 
	{ 
		CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues(); 
		Arrays.asList( 
				HttpMethod.OPTIONS, 
				HttpMethod.PUT, 
				HttpMethod.GET, 
				HttpMethod.DELETE, 
				HttpMethod.POST
		)
		.forEach(
				m -> corsConfiguration.addAllowedMethod(m)
		); 
		corsConfiguration.addAllowedOrigin("*"); 
		routePredicateHandlerMapping.setCorsConfigurations(
			new HashMap<String, CorsConfiguration>() 
			{
				{ 
					put("/**", corsConfiguration); 
				}
			}
		);
		return corsConfiguration; 
	}

	public static void main(String[] args) {
		SpringApplication.run(GatewayServiceApplication.class, args);
	}
	
	@Configuration
	@ComponentScan(basePackages = "com.ubagroup.utils")
	class Config{
		
//		@Bean
//		CorsFilter corsFilter() 
//		{
//			PathPatternParser pathParser = new PathPatternParser();
////			pathParser.
//		    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(pathParser);
//		    final CorsConfiguration config = new CorsConfiguration();
//		    config.setAllowCredentials(true);
//		    config.addAllowedOrigin("*");
//		    config.addAllowedHeader("*");
//		    config.addAllowedMethod("OPTIONS");
//		    config.addAllowedMethod("GET");
//		    config.addAllowedMethod("PUT");
//		    config.addAllowedMethod("POST");
//		    config.addAllowedMethod("DELETE");
//		    config.addAllowedMethod("HEAD");
//
//		    source.registerCorsConfiguration("/**", config);
//		    return new CorsFilter(source);
//		}
		
		
	}
}
